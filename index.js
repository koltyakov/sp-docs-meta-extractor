// @ts-check

const { PnpNode } = require('sp-pnp-node');
const { AuthConfig } = require('node-sp-auth-config');
const { sp, Web } = require('@pnp/sp');

(async () => {

  const { siteUrl, authOptions } = await new AuthConfig({
    configPath: './config/private.json'
  }).getContext();

  const fetchClientFactory = () => new PnpNode({ siteUrl, authOptions });

  sp.setup({
    sp: { fetchClientFactory }
  });

  const web = new Web(siteUrl);
  const docLibRelPath = `${siteUrl.split('/').slice(3).join('/')}/Shared Documents`;

  const list = web.getList(docLibRelPath);

  const items = await list.items
    .select('*,File/*,Folder/*,RoleAssignments/*,Author/Title,Editor/Title')
    .expand('File,Folder,RoleAssignments,Author,Editor')
    .top(500).get();

  console.log(items);

})()
  .catch(console.error);
