# SP Docs Metadata Extractor sample

## Restore dependencies

```bash
npm ci
```

## Run the sample

```bash
npm run start
```

Provide siteUrl and creads.

### Result

Gets metadata

```javascript
[{ 'odata.type': 'SP.Data.Shared_x0020_DocumentsItem',
    'odata.id': 'f4ce5963-1dec-4014-873d-75932ed039fe',
    'odata.etag': '"1"',
    'odata.editLink': 'Web/Lists(guid\'77b6cf2f-b4f8-49a9-a203-f1363a8fa117\')/Items(83)',
    'RoleAssignments@odata.navigationLinkUrl': 'Web/Lists(guid\'77b6cf2f-b4f8-49a9-a203-f1363a8fa117\')/Items(83)/RoleAssignments',
    RoleAssignments: 
     [ [Object],
       [Object],
       [Object],
       [Object],
       [Object],
       [Object],
       [Object],
       [Object],
       [Object],
       [Object] ],
    'File@odata.navigationLinkUrl': 'Web/Lists(guid\'77b6cf2f-b4f8-49a9-a203-f1363a8fa117\')/Items(83)/File',
    File: 
     { 'odata.type': 'SP.File',
       'odata.id': 'https://contoso.sharepoint.com/sites/site/_api/Web/Lists(guid\'77b6cf2f-b4f8-49a9-a203-f1363a8fa117\')/Items(83)/File',
       'odata.editLink': 'Web/Lists(guid\'77b6cf2f-b4f8-49a9-a203-f1363a8fa117\')/Items(83)/File',
       CheckInComment: '',
       CheckOutType: 2,
       ContentTag: '{7AA9F410-E373-4F11-9458-C5DBED46A25D},1,1',
       CustomizedPageStatus: 0,
       ETag: '"{7AA9F410-E373-4F11-9458-C5DBED46A25D},1"',
       Exists: true,
       IrmEnabled: false,
       Length: '1',
       Level: 2,
       LinkingUri: null,
       LinkingUrl: '',
       MajorVersion: 0,
       MinorVersion: 1,
       Name: '4.js',
       ServerRelativeUrl: '/sites/site/Shared Documents/src/1/2/3/4.js',
       TimeCreated: '2019-08-30T07:08:46Z',
       TimeLastModified: '2019-08-30T07:08:46Z',
       Title: null,
       UIVersion: 1,
       UIVersionLabel: '0.1',
       UniqueId: '7aa9f410-e373-4f11-9458-c5dbed46a25d' },
    'Folder@odata.navigationLinkUrl': 'Web/Lists(guid\'77b6cf2f-b4f8-49a9-a203-f1363a8fa117\')/Items(83)/Folder',
    FileSystemObjectType: 0,
    Id: 83,
    ServerRedirectedEmbedUri: null,
    ServerRedirectedEmbedUrl: '',
    ContentTypeId: '0x0101002FC8EAC532C58045A4D64A7556AA8D0E',
    ComplianceAssetId: null,
    Title: null,
    PublishingStartDate: null,
    PublishingExpirationDate: null,
    MediaServiceAutoTags: null,
    ID: 83,
    Created: '2019-08-30T07:08:46Z',
    AuthorId: 8,
    Modified: '2019-08-30T07:08:46Z',
    EditorId: 8,
    OData__CopySource: null,
    CheckoutUserId: null,
    OData__UIVersionString: '0.1',
    GUID: '56e4e3bc-fd55-46d8-8278-f33a61a536d7' }]
```